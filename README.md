# Programación declarativa y funcional

## 1. Programación Declarativa. Orientaciones y pautas para el estudio (1998, 1999, 2001)

```plantuml
@startmindmap
<style>
mindmapDiagram {
  Linecolor black
  .titulo {
    BackgroundColor #A9DCDF
  }
  .sub1 {
    BackgroundColor #B4A7E5
  }
  .sub2 {
    BackgroundColor #ADD1B2
  }
}
</style>

* Programación Declarativa. Orientaciones \ny pautas para el estudio (1998, 1999, 2001) <<titulo>>

**_ es un
*** Paradigma de programacion <<sub1>>

**** Surge <<sub2>>
*****_ para
****** Resolver 
*******_ los
******** problemas 
*********_ de la
********** programacion imperativa
***********_ como
************ Exceso de detalles 
*************_ en
************** Calculos deseados
************** Asignacion de memoria

**** Permite <<sub2>>
*****_ tener
****** Programas cortos
*******_ que son
******** Faciles de realizar
*********_ Sin asignaciones de
**********  Memoria manual
******** Faciles de leer
*********_ con
********** Codigo limpio
***********_ y
************ creativo
******** Faciles de depurar
*********_ con
********** Codigo legible 
***********_ y 
************ eficiente


**** Evita <<sub2>> 
*****_ problemas con la
****** Asignacion de memoria
*******_ a cada
******** Varible
********* Utilizada

*****_ problemas con la
****** Gestion de la memoria
*******_ liberacion de
******** Espacios en memoria
********* Utilizada
**********_ por las
*********** Variables
************_ utilizadas en el
************* Programa

*****_ la creacion de la
****** Programas complejos
*******_ se evita el uso de
******** malas practicas
*********_ que existen en la
********** Programacion imperativa

**** Variantes <<sub2>>

***** Programacion Funcional <<sub1>>
******_ Se basa en la
******* Descripcion 
********_ de 
********* Funciones matematicas
**********_ Se tienen
*********** Datos de Entrada
************_ que son
************* Los argumentos
*********** Datos de Salida
************_ que son
************* El resultado de la funcion
**********_ Se utilizan
*********** Reglas de simplificacion
****** Ventajas
******* Funciones de orden superior
******** Funciones 
*********_ que actuan sobre
********** Funciones 
*********_ y no solo sobre
********** Datos primitivos
******* Evaluacion perezosa
********_ Solo se evaluan
********* Valores
**********_ que se requieren para 
*********** Calculos posteriores
******* Manejo de memoria dinamico
********_ No es necesario
********* Gestionarla
**********_ de forma
*********** Detallada
************_ como sucede en la 
************* Programacion imperativa

***** Programacion Logica <<sub1>>
******_ Se basa en la
******* Logica
********_ especificamente en
********* La logica de predicados de primer orden
**********_ Se utilizan los
*********** Predicados
************_ que son
************* Relaciones entre objetos
******_ los
******* Predicados
********_ no establecen un
********* Orden
**********_ entre
*********** Datos de entrada
*********** Datos de salida
**********_ nos permite ser
*********** Más declarativos
******_ Se realiza el
******* Computo 
********_ del codigo mediante un
********* Interprete
**********_ dado un
*********** Conjunto de relaciones y predicados
************_ mediante
************* Algoritmos de resolucion
**************_ intenta
*************** Demostrar predicados
@endmindmap 
```

## 2. Lenguaje de Programación Funcional (2015)

```plantuml
@startmindmap
<style>
mindmapDiagram {
  Linecolor black
  .titulo {
    BackgroundColor #A9DCDF
  }
  .sub1 {
    BackgroundColor #B4A7E5
  }
  .sub2 {
    BackgroundColor #ADD1B2
  }
}
</style>

* Lenguaje de Programación Funcional (2015)<<titulo>>

**_ basado en el
*** Paradigma Funcional <<sub1>>

**** Comparacion <<sub2>>
***** Paradigma Imperativo <<sub1>>
******_ en este paradigma
******* Un Programa
********_ es una
********* Serie de instrucciones
**********_ que se ejecutan de manera
*********** Ordenada
**********_ el codigo se ejecuta de
*********** Arriba hacia abajo
************_ Para realizar
************* Un computo

****_ En este paradigma \nencontramos 
***** Funciones matematicas <<sub2>>
******_ estas reciben
******* Parametros 
********_ de 
********* Entrada
**********_ y 
*********** retornan
************_ una
************* Salida
******_ Pueden ser consideradas
******* Constantes
********_ por que en ocasiones cuentan con
********* Valores fijos
**********_ el
*********** Valor
************_ que entra, es
************* Igual
**************_ al que
*************** Sale

*****_ No existe la
****** Asignacion <<sub2>>
*******_ Por lo tanto, el
******** Estado del computo
*********_ del  
********** Modelo de von Neumann
*********** No existe
************_ en este
************* paradigma

******** Ciclo o bucle
*********_ para realizar
********** Iteraciones
***********_ tampoco exite, pero \n en su lugar se usa
************ La Recursion

*****_ Las
****** Variables <<sub2>>
*******_ existen, pero solo sirven \n para referirse a
******** Los valores 
*********_ de
********** Entrada
***********_ de las
************ Funciones
*************_ y no como
************** Espacios en memoria 
***************_ que pueden ser 
**************** Utilizados 
*****************_ en 
****************** Cualquier momento

***** Ventajas <<sub2>>
******_ los
******* Programas
********_ solo dependen de los
********* Parametros de entrada
**********_ se conoce como
*********** Transparencia \n referencial
************ Misma entrada
*************_ genera una
************** Misma salida
************ El orden 
*************_ del 
************** Computo  
***************_ no es 
**************** relevante
******_ Todo gira entorno a las
******* Funciones
********_ y al
********* Cálculo lambda
**********_ que fue
*********** Desarrollado
************_ por 
************* Alonzo Church 
**************_ y 
*************** Stephen Kleene
****** Currificacion
*******_ una
******** Funcion
*********_ con varios
********** Parametros de entrada
***********_ devuelve
************ Diferentes salidas o funciones
*************_ es una
************** Aplicacion parcial

***** Evaluacion perezosa <<sub2>>
******_ consiste en una
******* Evaluacion estricta
********_ primero de manera 
********* Interna
**********_ para finalizar con lo más 
*********** Externo

******* Evaluacion no estricta
********_ primero de manera 
********* Externa
**********_ para finalizar con lo más 
*********** Interno

******* Memoizacion
********_ consiste en
********* Almacenar
**********_ el 
*********** Valor 
************_ de una
************* Expresion 
**************_ que ya ha sido
*************** Evaluada
**************** previamente
 
@endmindmap 
```